
'use strict'

if (process.env.NODE_ENV === 'production') {
  module.exports = require('./kshark-sdk.cjs.production.min.js')
} else {
  module.exports = require('./kshark-sdk.cjs.development.js')
}
